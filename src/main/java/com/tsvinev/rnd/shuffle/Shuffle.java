package com.tsvinev.rnd.shuffle;

import java.lang.reflect.Array;

public class Shuffle {
	
	public static <T> T[] shuffle(T[] in) {
		if(null == in || 1 == in.length) {
			return in;
		}
		Class<? extends Object[]> inClass = in.getClass();
		if(!inClass.isArray()) {
			throw new IllegalArgumentException("Expected array parameter");
		}
		Class<?> componentType = inClass.getComponentType();
		@SuppressWarnings("unchecked")
		T[] out = (T[])(Array.newInstance(componentType, in.length));
		return shuffle(in, out);
	}
	
	private static <T> T[] shuffle(T[] in, T[] out) {
		// implement me
		return out;
	}

}
