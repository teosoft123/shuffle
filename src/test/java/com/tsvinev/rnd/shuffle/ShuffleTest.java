package com.tsvinev.rnd.shuffle;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ShuffleTest {

	private static final String TEST_STRING_1 = "test string 1";
	private static final String[] TEST_ARRAY_1 = new String[] {TEST_STRING_1};

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void mustHandleNullInput() {
		assertNull(Shuffle.shuffle(null));
	}

	@Test
	public void mustHandleSingleElementInput() {
		assertArrayEquals(TEST_ARRAY_1, Shuffle.shuffle(TEST_ARRAY_1));
	}
	
	/**
	 * While testing null and one element array is trivial,
	 * how do we test a real use case scenario? With multiple elements?
	 * One, the resulting array must have the same elements as the source array.
	 * Two, they might (or, with intuitively lower probability, might not be) in a different order
	 * Three, the test might want to measure the randomness of the shuffle algorithm - but, 
	 * in this case we're actually measure the randomness of the random number generator inevitably used in implementation,
	 * and this is a test in its own rights, so testing randomness should not be a part of our test
	 * Now, the implementation might be insufficient, like one that just switches neighboring elements. How do we detect that?
	 * The implementation switching neighboring elements will be detected by shuffling twice and expecting the original array.
	 * But these all are particular cases, how do we actually measure the shuffle algorithm quality, this is the question.
	 * At the end, the implementation is in Wikippedia: http://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle 
	 */
	@Test
	public void mustHandleRandomArrayInput() {
//		String[] shuffle = Shuffle.shuffle(TEST_ARRAY_1);
//		assertNotNull(shuffle);
//		assertEquals(1, shuffle.length);
//		shuffle = Shuffle.shuffle(new String[] { "test string" });
//		assertEquals(TEST_STRING_1, shuffle[0]);
	}

}
